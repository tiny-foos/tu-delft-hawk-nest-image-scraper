# TU Delft Hawk nest image scraper

This script periodically fetches image of the hawk nest, compares with previously downloaded
image and, if different, uploads to the Amazon S3 storage. 
Images are organized in directories, with root directory containing two directories, `inside` and `outside`.

Further, each directory will contain (currently only inside images are stored) directory for each day
with name in format `YYYYMMDD` and then each directory contains images with name in format `HHMMSS.jpg`

## Setup

### 1. Prerequisites

1. `nvm` to better handle different node versions
2. `node v15.0.1`
3. `yarn` packager manager

### 2. Global AWS Credentials

Create a file at `~/.aws/credentials`. Content should be like this:

```
[default]
aws_access_key_id = <KEY_ID>
aws_secret_access_key = <ACCESS_KEY>
```

### 3. Environment variables
Copy `example.env` to the `.env` file and set correct values. Delete those `optional` keys that you don't want to set 

### 4. Finishing setup

After prerequisites are installed and credentials file is created, run `yarn`
to install all required packages


## Usage

### Simple usage
Start script with `yarn start`

### Running script "forever"
Script can be run "forever" using `PM2` (https://github.com/Unitech/pm2). 
This can be installed in one of those ways : 
* with `yarn` by executing `yarn global add pm2`
* with `npm` by executing `npm install pm2 -g` or
* executing `wget -qO- https://getpm2.com/install.sh | bash` (if there is no yarn/npm available on the system).

1. Now just enter the application directory and run `pm2 start index.js`. App is now daemonized, monitored and kept alive forever.
2. To run script on system boot, execute `pm2 save` and then `pm2 startup` (if this fails, try to specify system like : `pm2 startup ubuntu`). Follow instructions for setting PATH variable


NOTE: It is possible that yarn will not install pm2 binary in system PATH (check ~/.yarn/bin). So to execute previous commands,
use full path to the pm2, like this : `~/.yarn/bin/pm2 start index.js`

NOTE 2: Basic commands for pm2 (for additional commands, check github link)

* `~/.yarn/bin/pm2 ls` lists all running scripts executed through pm2
* `~/.yarn/bin/pm2 monit` opens up monitor for running scripts (logs, memory,...)
* `~/.yarn/bin/pm2 delete [id]` deletes running script