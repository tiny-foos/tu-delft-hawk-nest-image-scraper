// Import required AWS SDK clients and commands for Node.js
const { S3Client, PutObjectCommand } = require("@aws-sdk/client-s3");
const CONFIG = require("./config.js").CONFIG;

// Create Amazon S3 service client object.
const s3 = new S3Client({ region: CONFIG.s3.region });

// Create and upload the object to the specified Amazon S3 bucket.
const uploadFile = async (imageBuffer, fileName, keyPrefix = "noCategory") => {
  // Set the parameters.
  const uploadParams = {
    Bucket: CONFIG.s3.bucketName,
    // Specify the name of the new object. For example, 'index.html'.
    // To create a directory for the object, use '/'. For example, 'myApp/package.json'.
    Key: `${keyPrefix}/${fileName}`,
    // Content of the new object.
    Body: imageBuffer
  };

  await s3.send(new PutObjectCommand(uploadParams));
};

exports.uploadFile = uploadFile;