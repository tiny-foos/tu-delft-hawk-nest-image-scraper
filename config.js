require("dotenv").config()

const CONFIG = {};

// S3 settings
let bucketName = process.env.S3_BUCKET;
let s3Region = process.env.S3_REGION;

if (bucketName && bucketName.length && bucketName.length > 0 &&
    s3Region && s3Region.length && s3Region.length > 0){
  CONFIG.s3 = {
    bucketName,
    region: s3Region
  }
}else{
  console.log("Some (or all?) S3 env variables not set (S3_BUCKET, S3_REGION)", "ERROR");
  process.exit(-1);
}

// Fetch interval
let fetchInterval = parseInt(process.env.APP_FETCH_INTERVAL);
if (isNaN(fetchInterval) || fetchInterval < 5){
  fetchInterval = 60;
}
// Image url of the inside and outside of the nest
let insideNestImageURL = process.env.INSIDE_IMAGE_URL;
let outsideNestImageURL = process.env.OUTSIDE_IMAGE_URL;
if (!insideNestImageURL || !outsideNestImageURL ||
  !(insideNestImageURL && insideNestImageURL.length && insideNestImageURL.length > 0 && insideNestImageURL.startsWith("http")) ||
  !(outsideNestImageURL && outsideNestImageURL.length && outsideNestImageURL.length > 0 && outsideNestImageURL.startsWith("http"))){
  console.log("Image URL to the inside/outside of the nest are not set (INSIDE_IMAGE_URL, OUTSIDE_IMAGE_URL", "ERROR");
  process.exit(-1);
}

// Last image name
let lastImageFileName = process.env.LAST_IMAGE_FILE_NAME;
if (!lastImageFileName || !lastImageFileName.length || lastImageFileName.length === 0){
  lastImageFileName = "last.jpg";
}
CONFIG.app = {
  fetchInterval,
  insideNestImageURL,
  outsideNestImageURL,
  lastImageFileName
};

// Sentry
let sentryDSN = process.env.SENTRY_DSN;
if (!sentryDSN || !sentryDSN.length || sentryDSN.length === 0){
  sentryDSN = '';
}
CONFIG.sentry = {
  dsn: sentryDSN
}

exports.CONFIG = CONFIG;