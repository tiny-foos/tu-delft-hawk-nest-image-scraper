const Sentry = require("@sentry/node");
const { writeFile, readFile, copyFile, unlink } = require("fs/promises");
const fetch = require("node-fetch");
const md5File = require("md5-file");
const { uploadFile } = require("./awsFunctions.js");

const downloadImage = async (url, fileName) => {
  const response = await fetch(url);
  const buffer = await response.buffer();
  await writeFile(`./tmp/${fileName}`, buffer);
}

const compareIfImagesAreDifferent = async (newImageFileName, lastImageFileName) => {
  try{
    const newImageMd5 = md5File.sync(`./tmp/${newImageFileName}`);
    const lastImageMd5 = md5File.sync(`./tmp/${lastImageFileName}`);
    return newImageMd5 !== lastImageMd5;
  }catch (e) {
    console.log("Failed to get md5 of new/last image", "ERROR");
    Sentry.captureException(e);
  }

  return true;
}

const replaceImage = async (oldImageFileName, newImageFileName) => {
  try{
    await copyFile(`./tmp/${newImageFileName}`, `./tmp/${oldImageFileName}`);
  }catch (e) {
    console.log(`Failed to replace old image with the new one. Old image : ${oldImageFileName}; New Image : ${newImageFileName}`, 'ERROR')
    Sentry.captureException(e);
  }
}

const uploadImage = async (imageFileName, location = "inside") => {
  const imageBuffer = await readFile(`./tmp/${imageFileName}`);
  await uploadFile(imageBuffer, imageFileName, location);
}

const deleteImage = async (imageFileName) => {
  try{
    await unlink(`./tmp/${imageFileName}`);
  }catch (e) {
    console.log(`Failed to delete image from temporary directory [${imageFileName}]`, "ERROR");
    Sentry.captureException(e);
  }
}

exports.downloadImage = downloadImage;
exports.compareIfImagesAreDifferent = compareIfImagesAreDifferent;
exports.replaceImage = replaceImage;
exports.uploadImage = uploadImage;
exports.deleteImage = deleteImage;