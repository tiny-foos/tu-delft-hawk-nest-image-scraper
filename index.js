require("./init.js");
const CONFIG = require("./config.js").CONFIG;
const Sentry = require("@sentry/node");
const { downloadImage, compareIfImagesAreDifferent, uploadImage, replaceImage, deleteImage } = require("./imageOperationFunctions.js");
const { getFormattedDateTimeObject } = require("./utilFunctions");

let insideImagesIntervalHandle;

const insideNestScrapper = async () => {
  pauseScraper();
  try{
    // 1. Download image locally
    const formattedDateTime = getFormattedDateTimeObject();
    const newImageFileName = `${formattedDateTime.timePart}.jpg`;
    await downloadImage(CONFIG.app.insideNestImageURL, newImageFileName);

    // 2. Compare image to the previously downloaded image
    const imagesAreDifferent = await compareIfImagesAreDifferent(newImageFileName, CONFIG.app.lastImageFileName);

    // 3. If new image is different from the previous one, replace old one and upload to the S3
    if (imagesAreDifferent){
      await replaceImage(CONFIG.app.lastImageFileName, newImageFileName);
      const locationPath = `inside/${formattedDateTime.datePart}`;
      await uploadImage(newImageFileName, locationPath);
      console.log("New image uploaded");
    }else{
      // otherwise, there was an error or images are the same, skip this image and if there was an error, it is reported
      console.log("\tImages are not different - do not upload image this round");
    }

    //4. Finally, delete downloaded image
    await deleteImage(newImageFileName);
  }catch (e) {
    console.log(`Error in the main interval handler: ${e.toString()}`, "ERROR");
    Sentry.captureException(e);
  }
  startScraper();
}

const pauseScraper = () => {
  clearInterval(insideImagesIntervalHandle);
}

const startScraper = () => {
  insideImagesIntervalHandle = setInterval(insideNestScrapper, CONFIG.app.fetchInterval*1000);
}

startScraper();