// Override console.log with custom function
const { getDateTimeTimestamp } = require("./utilFunctions");
const originalConsoleLog = console.log;

const loggingFunction = (message, type="INFO") => {
  originalConsoleLog(`${getDateTimeTimestamp()} [${type}] ${message}`);
};
console.log = loggingFunction;

console.log("Loading configuration");
const CONFIG = require("./config.js").CONFIG;

console.log("Loading sentry module");
const Sentry = require("@sentry/node");
Sentry.init({ dsn: CONFIG.sentry.dsn });





