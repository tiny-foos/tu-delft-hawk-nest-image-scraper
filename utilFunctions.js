const addLeadingZero = (number) => {
  return number < 10 ? `0${number}` : number;
}

const getFormattedDateTimeObject = () => {
  const date = new Date();
  const year = date.getFullYear();
  const month = date.getMonth() + 1;
  const day = date.getDate();
  const hours = date.getHours();
  const minutes = date.getMinutes();
  const seconds = date.getSeconds();

  return {
    datePart: `${year}${addLeadingZero(month)}${addLeadingZero(day)}`,
    timePart: `${addLeadingZero(hours)}${addLeadingZero(minutes)}${addLeadingZero(seconds)}`
  }
}

const getDateTimeTimestamp = () => {
  const date = new Date();
  const dateTimestamp = `${addLeadingZero(date.getDate())}.${addLeadingZero(date.getMonth())}.${date.getFullYear()}`;
  const timeTimestamp = `${addLeadingZero(date.getHours())}:${addLeadingZero(date.getMinutes())}:${addLeadingZero(date.getSeconds())}`;
  return `${dateTimestamp} ${timeTimestamp}`;
}

exports.getFormattedDateTimeObject = getFormattedDateTimeObject;
exports.getDateTimeTimestamp = getDateTimeTimestamp;
